//= require jquery
function show_result(data){ 
 let analyze_result = document.getElementById("analyze_result");
 //let chart_temp = document.getElementById("chart_temp"); 
 number_days = document.getElementById("number_days").value;
 if (data.max_temp==null){analyze_result.innerHTML = "<strong>Отчёт</strong>"+"<br>"+data.analysis}
 else{
 analyze_result.innerHTML = "<strong>Отчёт за последние "+number_days+ " дн.</strong>"+"<br>"+data.max_temp+"<br>"+data.min_temp+"<br>"+data.h_pres+"<br>"+data.l_pres+"<br>"+data.pulse+"<br>"+data.satur;
 var chart_temp = Chartkick.charts["chart_temp"];
 var chart_h_pres = Chartkick.charts["chart_h_pres"];
 var chart_l_pres = Chartkick.charts["chart_l_pres"];
 var chart_pulse = Chartkick.charts["chart_pulse"];
 var chart_satur = Chartkick.charts["chart_satur"];
 //console.log(chart_pres);
 chart_temp.updateData(data.data_chart_temp);
 chart_pulse.updateData(data.data_chart_pulse);
 chart_satur.updateData(data.data_chart_satur);
 chart_h_pres.updateData(data.data_chart_h_pres);
 chart_l_pres.updateData(data.data_chart_l_pres);
 console.log(chart_temp.data[0]);
 //chart_temp.innerHTML= //"<%= line_chart [{name: "Температура", data: "+ data.data_chart_temp +"}], min: 35, max: 40 %>";
}
}
 
function show_filter(data){
    let date_from = document.getElementById("filter_date_from").value;
    let date_to= document.getElementById("filter_date_to").value;
    let doctor_filter=document.getElementById("filter_doctor");
    let doctor=document.getElementById("filter_doctor").options[doctor_filter.selectedIndex].text;
    let table= document.querySelectorAll("#date");
    let table_doctor=document.querySelectorAll("#doctor");
    console.log(doctor);
    if (date_from!="" && date_to!="" && doctor=='Все'){
    table.forEach((element) =>{
        //console.log(Date.parse(element.textContent));
        //console.log(Date.parse (date_from));
        if ((Date.parse(element.textContent) < Date.parse (date_from)) || (Date.parse(element.textContent) > Date.parse (date_to))){
        element.parentElement.style.display="none";
    }
    else {element.parentElement.style.display="";}
})}

    if (date_from!="" && date_to!="" && doctor!='Все'){
    table.forEach((element) =>{
        console.log(Date.parse(element.textContent));
        console.log(Date.parse (date_from));
        if ((Date.parse(element.textContent) < Date.parse (date_from)) || (Date.parse(element.textContent) > Date.parse (date_to))){
        element.parentElement.style.display="none";
    }
    else {element.parentElement.style.display="";}
    })

    table_doctor.forEach((element) =>{
        //console.log(Date.parse(element.textContent));
        //console.log(doctor);
        if (element.textContent!=doctor){
            if (element.parentElement.style.display==""){
               element.parentElement.style.display="none";
            }
    }
    })
}

    if (date_from=="" && date_to=="" && doctor!='Все'){
        table_doctor.forEach((element) =>{
            //console.log(Date.parse(element.textContent));
            //console.log(Date.parse (date_from));
            if (element.textContent!=doctor){
            element.parentElement.style.display="none";
        }
        else {element.parentElement.style.display="";}
    })}
    if (date_from=="" && date_to=="" && doctor=='Все'){
        table_doctor.forEach((element) =>{
            element.parentElement.style.display="";
    })}
}

function user_search(data){
    let username = document.getElementById("username_search").value;
    let table= document.querySelectorAll("#username");
    table.forEach((element) =>{
        //console.log(element.textContent);
        if (element.textContent.indexOf(username,0)==-1){
               element.parentElement.style.display="none";
            }
        else {element.parentElement.style.display="";}
    })
}

function user_search_surname(data){
    let surname = document.getElementById("surname_search").value;
    let table= document.querySelectorAll("#surname");
    table.forEach((element) =>{
        //console.log(element.textContent);
        if (element.textContent.indexOf(surname,0)==-1){
               element.parentElement.style.display="none";
            }
        else {element.parentElement.style.display="";}
    })
}

$(document).ready(function(){
    $("#filter_visits").bind("ajax:success",
    function(event) {console.log(event.detail[0]); show_filter(event.detail[0])})
})

$(document).ready(function(){
    $("#analyze").bind("ajax:success",
    function(event) {console.log(event.detail[0]); show_result(event.detail[0])})
})

$(document).ready(function(){
    $("#username_search").keyup("ajax:success",
    function(event) {console.log(event.detail[0]); user_search(event.detail[0])})
})

$(document).ready(function(){
    $("#surname_search").keyup("ajax:success",
    function(event) {console.log(event.detail[0]); user_search_surname(event.detail[0])})
})


function find_patients_helper(data){
    let name_field = document.getElementById("name");
    let patr_field = document.getElementById("patr");
    let surname = document.getElementById("surname").value;
    let patients = document.getElementById("hidden").textContent;
    let start = patients.indexOf(surname);
    let dot = ',';
    let finish = patients.indexOf(dot,start);
    if (finish==-1) {finish=patients.indexOf(']',start)};
    console.log(start);
    patients=patients.slice(start, finish);
    patients=patients.slice(0,patients.length-1);
    let name=patients.slice(patients.indexOf(" ")+1, patients.indexOf(" ", patients.indexOf(" ")+1));
    let patr=patients.slice(patients.indexOf(" ", patients.indexOf(" ")+1)+1);
    name_field.value=name;
    patr_field.value=patr;
    console.log(patr);

}

$(document).ready(function(){
    $("#name").focus("ajax:success",
    function(event) {console.log(event.detail[0]); find_patients_helper(event.detail[0])})
})

function find_patients_helper_1(data){
    let name_field = document.getElementById("name1");
    let patr_field = document.getElementById("patr1");
    let surname = document.getElementById("surname1").value;
    let patients = document.getElementById("hidden").textContent;
    let start = patients.indexOf(surname);
    let dot = ',';
    let finish = patients.indexOf(dot,start);
    if (finish==-1) {finish=patients.indexOf(']',start)};
    console.log(start);
    patients=patients.slice(start, finish);
    patients=patients.slice(0,patients.length-1);
    let name=patients.slice(patients.indexOf(" ")+1, patients.indexOf(" ", patients.indexOf(" ")+1));
    let patr=patients.slice(patients.indexOf(" ", patients.indexOf(" ")+1)+1);
    name_field.value=name;
    patr_field.value=patr;

}

$(document).ready(function(){
    $("#name1").focus("ajax:success",
    function(event) {console.log(event.detail[0]); find_patients_helper_1(event.detail[0])})
})

function create_block(data){
    let notice = document.getElementById("notice");
    notice.textContent=data.notice;

}

$(document).ready(function(){
    $("#create_block").bind("ajax:success",
    function(event) {console.log(event.detail[0]); create_block(event.detail[0])})
})