# frozen_string_literal: true

DOCTOR = 'doctor'
class MainController < ApplicationController
  include MainHelper
  before_action :parse_params, only: %i[view_all]
  before_action :parse_params1, only: :view_vis
  before_action :check_login, only: %i[select view_all view_date view_vis]

  def select
    if @current_user.role==DOCTOR
      doctor_visits = Visit.all.select do |record|
        record.user!= "" && record.doctor == current_user.username
      end.select{|record| record.user!=nil}.pluck(:user).uniq
      @patients=doctor_visits.map do |record|
        record=User.find_by_username(record).surname+" "+User.find_by_username(record).name+" "+User.find_by_username(record).patr
      end
    end
  end

  def view_all
    if @act == 'view_all'
      p 'view_all'
      redirect_to data_path
    elsif @act == 'find_date'
      p 'find_date'
      @data = Datum.all.select do |record|
        record.username == @current_user.username && record.data.to_date.strftime('%Y-%m-%d') == @date
      end.sort_by(&:data).reverse!
    elsif @act == 'find_pat' && @current_user.role == DOCTOR
      if @name != '' && @surname != '' && @patr != ''
        p 'find_pat'
        user = User.find_by_name_and_surname_and_patr(@name, @surname, @patr)
        if !user.nil?
          @@user_for_analysis=user
          @data = Datum.all.select { |record| record.username == user.username }.sort_by(&:data).reverse!
          @date = "#{user.surname} #{user.name} #{user.patr}"
          if !@data.nil?
            @data_chart_temp={}
            @data.map do |datum|
             @data_chart_temp[datum.data-10800]=datum.temp
            end
            @data_chart_h_pres={}
            @data.map do |datum|
              @data_chart_h_pres[datum.data-10800]=datum.h_pres
            end
            @data_chart_l_pres={}
            @data.map do |datum|
             @data_chart_l_pres[datum.data-10800]=datum.l_pres
            end
            @data_chart_pulse={}
            @data.map do |datum|
             @data_chart_pulse[datum.data-10800]=datum.pulse
            end
            @data_chart_satur={}
            @data.map do |datum|
             @data_chart_satur[datum.data-10800]=datum.satur
            end
          else
            @data_chart={}
            @data_chart_h_pres={}
            @data_chart_l_pres={}
            @analysis="За последнюю неделю записей в дневнике здоровья нет"
          end
        else
          redirect_to main_select_path, notice: t('session.login.user_not_found') end
      else
        redirect_to main_select_path, notice: t('main.select.enter_all_info')
      end
    else
      p 'new_record'
      redirect_to new_datum_path
    end
  end

  def view_vis
    user = User.find_by_username(@current_user.username)
    @date = "#{user.surname} #{user.name} #{user.patr}"
    if @act1 == 'view_last' && @current_user.role!=DOCTOR
      p 'view_last'
      #if @date_from != '' && @date_to!= ''
        @data = Visit.all.select do |record| record.user == @current_user.username && record.date.to_date <= Time.now end.sort_by(&:date).reverse
      #else
        ##redirect_to main_select_path, notice: t('main.select.enter_all_info')
      #end 
    elsif @act1 == 'view_last' && @current_user.role==DOCTOR
      if @name != '' && @surname != '' && @patr != ''
        user = User.find_by_name_and_surname_and_patr(@name, @surname, @patr)
        if !user.nil?
          @data = Visit.all.select { |record| record.user == user.username && record.date.to_date <= Time.now }.sort_by(&:date).reverse!
          @date = "#{user.surname} #{user.name} #{user.patr}"
        else
          redirect_to main_select_path, notice: t('session.login.user_not_found')
        end
      else
        redirect_to main_select_path, notice: t('main.select.enter_all_info')
      end
    elsif @act1 == 'view_fut' && @current_user.role!=DOCTOR
      p 'view_fut'
      @data = Visit.all.select do |record| record.user == @current_user.username && record.date >= Time.now end.sort_by(&:date)
    elsif @act1 == 'view_fut' && @current_user.role==DOCTOR
      p 'view_fut'
      redirect_to visits_path
    elsif @act1 == 'add_vis'
      p 'add_vis'
      redirect_to visits_path
    end
  end

  def view_date
    @actual_record = Datum.all.select do |record|
      record.username == @current_user.username
    end.sort_by(&:data).reverse!.take(1)
    p @actual_record
    respond_to do |f|
      f.html
      f.json do
        render json: { actual_record: @actual_record }
      end
    end
  end

  def filter
    render json: { data: @data}
  end

  def analyze
    @number_days = params[:number_days]
    p @@user_for_analysis.name
    data_week=Datum.all.select{|record| record.username == @@user_for_analysis.username && record.data<=Time.now+10800 &&record.data>=Time.now-(86400*@number_days.to_i)}
    if data_week!=[]
      temp_max=data_week.max_by{|temp| temp[:temp]}.temp
      temp_max_date=data_week.max_by{|temp| temp[:temp]}.data.strftime("%Y-%m-%d %H:%M")
      temp_min=data_week.min_by{|temp| temp[:temp]}.temp
      temp_min_date=data_week.min_by{|temp| temp[:temp]}.data.strftime("%Y-%m-%d %H:%M")
      h_pres_max=data_week.max_by{|pres| pres[:h_pres]}.h_pres
      h_pres_max_date=data_week.max_by{|pres| pres[:h_pres]}.data.strftime("%Y-%m-%d %H:%M")
      h_pres_min=data_week.min_by{|pres| pres[:h_pres]}.h_pres
      h_pres_min_date=data_week.min_by{|pres| pres[:h_pres]}.data.strftime("%Y-%m-%d %H:%M")
      l_pres_max=data_week.max_by{|pres| pres[:l_pres]}.l_pres
      l_pres_max_date=data_week.max_by{|pres| pres[:l_pres]}.data.strftime("%Y-%m-%d %H:%M")
      l_pres_min=data_week.min_by{|pres| pres[:l_pres]}.l_pres
      l_pres_min_date=data_week.min_by{|pres| pres[:l_pres]}.data.strftime("%Y-%m-%d %H:%M")
      pulse_max=data_week.max_by{|temp| temp[:pulse]}.pulse
      pulse_max_date=data_week.max_by{|temp| temp[:pulse]}.data.strftime("%Y-%m-%d %H:%M")
      pulse_min=data_week.min_by{|temp| temp[:pulse]}.pulse
      pulse_min_date=data_week.min_by{|temp| temp[:pulse]}.data.strftime("%Y-%m-%d %H:%M")
      satur_select=data_week.select{|record| record.satur<=95}
      @max_temp = "Максимальное значение температуры: #{temp_max}С (дата: #{temp_max_date})"
      @min_temp = "Минимальное значение температуры: #{temp_min}С (дата: #{temp_min_date})"
      @h_pres = "Систолическое АД изменялось в пределах: #{h_pres_min} мм рт.ст. (дата: #{h_pres_min_date}) - #{h_pres_max} мм рт.ст. (дата: #{h_pres_max_date})"
      @l_pres = "Диастолическое АД изменялось в пределах: #{l_pres_min} мм рт.ст. (дата: #{l_pres_min_date}) - #{l_pres_max} мм рт.ст. (дата: #{l_pres_max_date})"
      @pulse = "ЧСС изменялась в пределах: #{pulse_min} ударов/мин (дата: #{pulse_min_date}) - #{pulse_max} ударов/мин (дата: #{pulse_max_date})"
      if !(satur_select==[])
       @satur = "Сатурация была ниже нормы (<=95%): "
       satur_select.each do |record|
        @satur = @satur + record.satur.to_s + "% (дата: " + record.data.strftime("%Y-%m-%d %H:%M") + ")  "
       end
     else @satur = "Сатурация была в норме (>95%)"
     end
     @data_chart_temp={}
     data_week.map do |datum|
        @data_chart_temp[datum.data-10800]=datum.temp
     end
            @data_chart_h_pres={}
            data_week.map do |datum|
              @data_chart_h_pres[datum.data-10800]=datum.h_pres
            end
            @data_chart_l_pres={}
            data_week.map do |datum|
             @data_chart_l_pres[datum.data-10800]=datum.l_pres
            end
            @data_chart_pulse={}
            data_week.map do |datum|
             @data_chart_pulse[datum.data-10800]=datum.pulse
            end
            @data_chart_satur={}
            data_week.map do |datum|
             @data_chart_satur[datum.data-10800]=datum.satur
            end
   else 
    @analysis="Записей в дневнике здоровья за выбранный период нет"
    @data_chart_temp={}
    @data_chart_h_pres={}
    @data_chart_l_pres={}
    @data_chart_satur={}
    @data_chart_satur={}
   end
   render json: { data_chart_l_pres: @data_chart_l_pres, data_chart_pulse: @data_chart_pulse, data_chart_satur: @data_chart_satur, data_chart_temp: @data_chart_temp, data_chart_h_pres: @data_chart_h_pres, max_temp: @max_temp, min_temp: @min_temp, h_pres: @h_pres, l_pres: @l_pres, pulse: @pulse, satur: @satur, analysis: @analysis, data_chart_temp: @data_chart_temp }
  end

  private

  def parse_params
    @date = params[:date]
    @act = params[:act]
    #@date_from = params[:date_from]
    #@date_to = params[:date_to]
    @name = params[:name]
    @surname = params[:surname]
    @patr = params[:patr]
  end
  
  def parse_params1
    @date = ""
    @act1 = params[:act1]
    ##@date_from = params[:date_from]
    ##@date_to = params[:date_to]
    @name = params[:name]
    @surname = params[:surname]
    @patr = params[:patr]
  end
end
