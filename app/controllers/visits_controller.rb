class VisitsController < ApplicationController
  before_action :set_visit, only: %i[ show edit update destroy ]
  before_action :check_login, only: %i[index show new edit create update destroy new_block create_block]
  AdminID = 'admin'
  DoctorID = "doctor"
  # GET /visits or /visits.json
  def index
    if current_user.role!=DoctorID && current_user.username!=AdminID
      @doctors=User.all.select { |record| record.role == "doctor"}.sort_by(&:surname)
    @visits = Visit.all.select do |record|
                record.doctor!="" && record.user== ""  && record.date >= Time.now
              end.sort_by(&:date)
    elsif current_user.role==DoctorID
      @visits = Visit.all.select do |record|
        record.user!= "" && record.date >= Time.now && record.doctor == current_user.username
      end.sort_by(&:date)
    elsif current_user.username==AdminID
      @visits = Visit.all.select do |record|
        record.date >= Time.now
      end.sort_by(&:date)  
    end
  end

  # GET /visits/1 or /visits/1.json
  def show
  end

  # GET /visits/new
  def new
    @visit = Visit.new
  end

  # GET /visits/1/edit
  def edit
  end

    # POST /visits or /visits.json
    def create
      @visit = Visit.new(visit_params)
  
      respond_to do |format|
        if @visit.save
          format.html { redirect_to visit_url(@visit), notice: "Приём успешно создан." }
          format.json { render :show, status: :created, location: @visit }
        else
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @visit.errors, status: :unprocessable_entity }
        end
      end
    end
  
    def new_block
      
    end

    def create_block
      @date=params[:date]
      @doctor=params[:doctor]
      @time_start=DateTime.parse(params[:time_start])
      @time_end=DateTime.parse(params[:time_end])
      @duration=params[:duration]
        cnt=0
        p @time_start
      while (@time_start<@time_end && cnt<=5)
        p @time_start
        @visit = Visit.new
        @visit.date=@time_start
        @visit.doctor=@doctor
        @visit.user=''
        if !@visit.save
          @notice="Ошибка добавления записей"
          #redirect_to visits_new_block_path, notice:"Ошибка добавления записей"
          #return
        else 
          @notice="Записи успешно добавлены"
        end
        cnt=cnt+1
        @time_start=Time.at(@time_start.to_i+@duration.to_i*60)
        p @time_start
      end
      render json:{notice:@notice}
      #redirect_to visits_new_block_path
    end  

  # PATCH/PUT /visits/1 or /visits/1.json
  def update
    if @current_user.role!=DoctorID
    respond_to do |format|
      if @visit.update(user: @current_user.username)
          format.html { redirect_to visit_url(@visit), notice: "Вы успешно записались на приём." }
          p "add"
          format.json { render :show, status: :ok, location: @visit }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @visit.errors, status: :unprocessable_entity }
      end
    end
    end
    if @current_user.role==DoctorID
      respond_to do |format|
        if @visit.update(visit_params)
          format.html { redirect_to visit_path(@visit) , notice: "Данные сохранены"}
            p "add"
          format.json { render :show, status: :ok, location: @visit }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @visit.errors, status: :unprocessable_entity }
        end
      end
    end
  end


  def filter
    render json: { data: @data}
  end
  # DELETE /visits/1 or /visits/1.json
  def destroy
    if current_user.role!=DoctorID && current_user.username!=AdminID
    @visit.update(user: "")

    respond_to do |format|
      format.html { redirect_to main_select_path }
      format.json { head :no_content }
    end
  end
  if current_user.username==AdminID
    @visit.destroy

    respond_to do |format|
      format.html { redirect_to visits_path }
      format.json { head :no_content }
    end
  end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_visit
      @visit = Visit.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def visit_params
      params.require(:visit).permit(:date, :user, :compl, :res, :recom, :next_v, :doctor, :anamnesis, :diagnosis)
    end
end
