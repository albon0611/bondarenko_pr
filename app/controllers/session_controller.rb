# frozen_string_literal: true

class SessionController < ApplicationController
  skip_before_action :check_login, only: %i[login create]

  def login; end

  def create
    user = User.find_by(username: params[:username])

    if user&.authenticate(params[:password])
      p 'login'
      sign_in user
      redirect_to main_select_path
    else
      p 'incorrect username or password'
      redirect_to session_login_path, notice: t('session.login.user_not_found')
    end
  end

  def logout
    sign_out
    redirect_to root_path
  end
end
