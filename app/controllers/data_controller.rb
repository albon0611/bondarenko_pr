# frozen_string_literal: true

class DataController < ApplicationController
  before_action :set_datum, only: %i[show edit update destroy]
  before_action :check_login, only: %i[index show new edit create update destroy]
  DOCTOR_ID = 'doctor'
  # GET /data or /data.json
  def index
    @data = if @current_user.role != DOCTOR_ID
              Datum.all.select do |record|
                record.username == @current_user.username
              end.sort_by(&:data).reverse!
            else
              find_patients()
              Datum.all.select{|record| @patients.include?(record.username)}.sort_by(&:data).reverse!
            end        
    @filter_date_from = params[:filter_date_from]
    @filter_date_to = params[:filter_date_to]        
    if @filter_date_from!=nil && @filter_date_to!=nil
      @data=@data.select do 
        |record| record.data.to_date.strftime('%Y-%m-%d')>=params[:filter_date_from] && record.data.to_date.strftime('%Y-%m-%d')<=params[:filter_date_to] 
      end
    end 
    @data_chart_temp={}
    @data.map do |datum|
      @data_chart_temp[datum.data-10800]=datum.temp
    end
    @data_chart_h_pres={}
    @data.map do |datum|
      @data_chart_h_pres[datum.data-10800]=datum.h_pres
    end
    @data_chart_l_pres={}
    @data.map do |datum|
      @data_chart_l_pres[datum.data-10800]=datum.l_pres
    end
    @data_chart_pulse={}
    @data.map do |datum|
      @data_chart_pulse[datum.data-10800]=datum.pulse
    end
    @data_chart_satur={}
    @data.map do |datum|
      @data_chart_satur[datum.data-10800]=datum.satur
    end
    #@data.each {|datum| @data_date << datum.data}
  end

  # GET /data/1 or /data/1.json
  def show; end

  # GET /data/new
  def new
    @datum = Datum.new
  end

  # GET /data/1/edit
  def edit; end

  # POST /data or /data.json
  def create
    @datum = Datum.new(datum_params)
    @datum.username = @current_user.username
    respond_to do |format|
      if @datum.save
        p 'saved in data_db'
        format.html { redirect_to datum_url(@datum), notice: t('data.contr.suc_created') }
        format.json { render :show, status: :created, location: @datum }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /data/1 or /data/1.json
  def update
    if @datum.username == @current_user.username
      p 'true'

      respond_to do |format|
        if @datum.update(datum_params)
          format.html { redirect_to datum_url(@datum), notice: t('data.contr.suc_updated') }
          format.json { render :show, status: :ok, location: @datum }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @datum.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to data_path
    end
  end

  # DELETE /data/1 or /data/1.json
  def destroy
    if @datum.username == @current_user.username
      p 'true'
      @datum.destroy

      respond_to do |format|
        format.html { redirect_to data_url, notice: t('data.contr.suc_destroyed') }
        format.json { head :no_content }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_datum
    @datum = Datum.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def datum_params
    params.require(:datum).permit(:username, :data, :temp, :h_pres, :l_pres, :satur, :pulse, :comment)
  end

  def find_patients
    @patients = Visit.all.select do |record|
      record.user!= "" && record.doctor == current_user.username
    end.select{|record| record.user!=nil}.pluck(:user).uniq
  end
end
