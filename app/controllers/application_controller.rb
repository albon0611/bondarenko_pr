class ApplicationController < ActionController::Base
  include SessionHelper
include MainHelper
  
  before_action :check_login
  around_action :switch_locale
  
  private
  def check_login
   unless signed_in?
     redirect_to session_login_url
   end
  end
  
  def switch_locale(&action)
    locale = locale_from_url || I18n.default_locale
    I18n.with_locale locale, &action
  end
  
  def locale_from_url
    locale=params[:locale]
    
    return locale if I18n.available_locales.map(&:to_s).include?(locale)
  end
  
  def default_url_options
    {locale: I18n.locale}
  end  
end
