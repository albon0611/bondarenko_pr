# frozen_string_literal: true
ADMIN = 'admin'
class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy]
  skip_before_action :check_login, only: %i[new create]
  before_action :check_login, only: %i[new_doctor create_doctor show edit update destroy index]

  # GET /users or /users.json
  def index
    if @current_user.username==ADMIN
      @users = User.all
    end
  end

  # GET /users/1 or /users/1.json
  def show; end

  # GET /users/new
  def new
    @user = User.new
  end

  def new_doctor
    @user = User.new
    @user.role="doctor"
  end

  def create_doctor
    if (@current_user.username=="admin")
    @user = User.new(user_params)
    @user.role="doctor"
    respond_to do |format|
      if @user.save
        p 'saved'
        ##sign_in @user
        format.html { redirect_to user_url(@user), notice: t('users.contr.suc_created') }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  end

  # GET /users/1/edit
  def edit; end

  # POST /users or /users.json
  def create
    @user = User.new(user_params)
    @user.role=""
    respond_to do |format|
      if @user.save
        p 'saved'
        sign_in @user
        format.html { redirect_to user_url(@user), notice: t('users.contr.suc_created') }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1 or /users/1.json
  def update
    if @user.username == @current_user.username
      p 'user checked'
      respond_to do |format|
        if @user.update(user_params)
          Datum.all.select do |record|
            record.username == @current_user.username
          end.each do |record|
            record.update(username: user_params[:username],
                          data: record.data, temp: record.temp, h_pres: record.h_pres, l_pres: record.l_pres)
          end
          format.html { redirect_to user_url(@user), notice: t('users.contr.suc_updated') }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to users_url
    end
  end

  # DELETE /users/1 or /users/1.json
  def destroy
    if @user.username == @current_user.username && @current_user.role!='doctor'
      p 'user checked'
      @user.destroy
      cur_records = Datum.all.select { |record| record.username == @user.username }.each(&:delete)
      respond_to do |format|
        format.html { redirect_to users_url, notice: t('users.contr.suc_destroyed') }
        format.json { head :no_content }
      end
    else
      redirect_to users_url
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def user_params
    params.require(:user).permit(:username, :password, :polis, :birth, :name, :surname, :patr)
  end
end
