class Datum < ApplicationRecord
  #belongs_to :user
  validates :username, presence: true
  validates :data, presence: true
  validates :temp, presence: true
end
