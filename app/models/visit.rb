class Visit < ApplicationRecord
    validates :date, presence: true, uniqueness: true
end
