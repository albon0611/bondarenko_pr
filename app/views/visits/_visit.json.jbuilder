json.extract! visit, :id, :date, :user, :compl, :res, :recom, :next_v, :created_at, :updated_at
json.url visit_url(visit, format: :json)
