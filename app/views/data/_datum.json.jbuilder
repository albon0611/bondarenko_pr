json.extract! datum, :id, :username, :data, :temp, :h_pres, :l_pres, :created_at, :updated_at
json.url datum_url(datum, format: :json)
