Rails.application.routes.draw do
  resources :visits
  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    root 'session#login'
    get 'session/login'
    post 'session/create'
    get 'session/logout'
    get 'users/new_doctor'
    post 'users/create_doctor'
    resources :users
    resources :data
    resources :visit
    get 'visits/new_block'
    post 'visits/create_block'
    get 'visits/filter'
    get 'main/select'
    get 'main/analyze'
    get 'main/filter'
    get 'main/view_all'
    get 'main/view_date'
    get 'main/view_vis'
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
