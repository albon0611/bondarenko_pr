require 'test_helper'

class DatumTest < ActiveSupport::TestCase
  test 'save record' do # невозможно сохранить пустую запись
    test_record = Datum.new
    assert_not test_record.save
  end

  test 'new rec' do # проверка на сохранение корректной записи
    test_record = Datum.create username: 'alex', data: '2022-01-06 14:01:00 UTC', temp: '37.1'
    assert test_record.save
    assert Datum.find_by_data('2022-01-06 14:01:00 UTC')
  end

  test 'new incorrect record' do # невозможно сохранить запись с пустым полем temp
    test_record = Datum.create username: 'alexey1', data: '2022-01-06 14:01:00 UTC', temp: ''
    assert_not test_record.save
  end

test 'incorrect record 2' do # невозможно сохранить запись с пустым полем username
test_record = Datum.create username: '', data: '2022-01-06 14:01:00 UTC', temp: '37.1'
assert_not test_record.save
end

test 'incorrect record 3' do # невозможно сохранить запись с пустым полем data
    test_record = Datum.create username: 'alexey1', temp: '37.1'
    assert_not test_record.save
  end
end
