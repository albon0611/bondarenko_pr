require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'save record' do # невозможно сохранить пустую запись
    test_record = User.new
    assert_not test_record.save
  end

  test 'new rec' do # проверка на сохранение корректной записи
    test_record = User.create username: 'alex', password_digest: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    assert test_record.save
    assert User.find_by_username('alex')
  end

  test 'new incorrect record' do # невозможно сохранить запись с пустым полем username
    test_record = User.create password_digest: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    assert_not test_record.save
  end

  test 'incorrect record 2' do # невозможно сохранить запись с пустым полем password_digest
    test_record = User.create username: 'alex', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    assert_not test_record.save
  end
  
  test 'duplicate record' do # невозможно сохранить пользователей с одинаковым полем username
    rec1 = User.create username: 'alex', password_digest: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    rec1.save
    rec2 = User.create username: 'alex', password_digest: '1234', name: 'Иван', surname: 'Керимов', patr: 'Валерьевич'
    assert_not rec2.save
  end
end
