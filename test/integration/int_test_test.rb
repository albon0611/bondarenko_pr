require 'test_helper'
DOCTOR="doctor"
class IntTestTest < ActionDispatch::IntegrationTest
  
=begin
  
  test 'unauthorized user will be redirected to login page' do #неавторизированный пользователь будет перенаправлен на стартовую страницу
    get main_select_url
    assert_redirected_to session_login_path
  end

  test 'user with incorrect credentials will be redirected to login page' do #несуществующий пользователь будет перенаправлен на стартовую страницу
    post session_create_url, params: { username: 'test1', password: '123' }
    assert_redirected_to session_login_path
  end

  test 'doctor with correct username and password will see the find_pat' do # пользователь 'doctor' получит доступ к записи пациента
    user = User.create username: DOCTOR, password: '1', name: 'Алексей', surname: 'Суриков', patr: 'Александрович'
    post session_create_url, params: { username: user.username, password: user.password }

    assert_redirected_to controller: :main, action: :select
    User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    Datum.create username: 'alex',data: '2022-01-06 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
    get main_view_all_url, params: { act: "find_pat", name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович' }
    assert_select '#data', 'Data: Бондаренко Алексей Александрович'
  end
  
  test 'incorrect find_pat' do # пользователь 'doctor' получит сообщение, если пользователь не будет найден
    user = User.create username: DOCTOR, password: '1', name: 'Алексей', surname: 'Суриков', patr: 'Александрович'
    post session_create_url, params: { username: user.username, password: user.password }

    assert_redirected_to controller: :main, action: :select
    User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    Datum.create username: 'alex',data: '2022-01-06 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
    get main_view_all_url, params: { act: "find_pat", name: 'Алексей', surname: 'Иванов', patr: 'Александрович' }
    assert_redirected_to main_select_path
    follow_redirect!
    assert_select 'div', 'User not found'
  end
  
  test 'not all data for find_pat' do # пользователь 'doctor' получит сообщение, если при попытке посмотреть данные по конкретному пользователю будут введены не все данные 
    user = User.create username: DOCTOR, password: '1', name: 'Алексей', surname: 'Суриков', patr: 'Александрович'
    post session_create_url, params: { username: user.username, password: user.password }

    assert_redirected_to controller: :main, action: :select
    User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    Datum.create username: 'alex',data: '2022-01-06 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
    get main_view_all_url, params: { act: "find_pat", name: 'Алексей', surname: '', patr: 'Александрович' }
    assert_redirected_to main_select_path
    follow_redirect!
    assert_select 'div', 'Enter all information'
  end

=end


  test '155555' do # пользователь 'doctor' получит сообщение, если при попытке посмотреть данные по конкретному пользователю будут введены не все данные 

    
    user = User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    post session_create_url, params: { username: user.username, password: user.password }

    User.create username: 'alex1', password: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    Datum.create username: 'alex1',data: '2022-01-06 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
    #assert_redirected_to controller: :main, action: :select
    patch "/data/980190963", params: { datum: { data: '2022-01-06 14:01:00 UTC', temp: '37.3', h_pres: '130', l_pres:'81' } }
    p Datum.find_by_data('2022-01-06 14:01:00 UTC').temp
    assert Datum.find_by_data('2022-01-06 14:01:00 UTC').temp!="37.3"
  end
end
