require 'test_helper'

class SessionControllerTest < ActionDispatch::IntegrationTest
  test 'incorrect data' do # незарегистрировванный пользователь не сможет войти
    post session_create_url, params: { username: 'aaa', password: '1' }
    assert_redirected_to session_login_path
    follow_redirect!
    assert_select "div", "User not found"
  end
  
  test 'correct data' do # зарегистрированный пользователь сможет перейти на страницу с выбором действия
    user=User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Суриков', patr: 'Александрович'
    post session_create_url, params: { username: user.username, password: user.password }
    assert_redirected_to main_select_path
  end
end
