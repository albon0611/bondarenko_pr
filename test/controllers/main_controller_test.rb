require 'test_helper'
DOCTOR='doctor'
class MainControllerTest < ActionDispatch::IntegrationTest
  def start
    user=User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Суриков', patr: 'Александрович'
    post session_create_url, params: { username: user.username, password: user.password }
    Datum.create username: 'alex',data: '2022-01-06 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
    Datum.create username: 'alex',data: '2022-01-07 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
  end

  test "should get select" do #пользователь получит доступ к странице с выбором действия
    start
    get main_select_url
    assert_response :success
    assert_select "h1", "Hello,  Суриков Алексей Александрович"
  end

  test "should get view_all" do #пользователь получит доступ к странице со своими записями
    start
    get main_view_all_url, params: { act: "view_all"}
    follow_redirect!
    assert_select "h1", "Records"
    assert_select "#surname", "Суриков"
    assert_select "#name", "Алексей"
    assert_select "#patr", "Александрович"
    assert_select "#date", '2022-01-07 14:01:00 UTC'
    assert_select "#temp", "37.2"
    assert_select "#h_pres", "130"
    assert_select "#l_pres", "81"
  end

  test "should get view_date" do #пользователь получит доступ к странице со своими записями за конкретную дату
    start
    get main_view_all_url, params: { act: "find_date", date: "2022-01-06"}
    assert_response :success
    assert_select "#data", "Data: 2022-01-06"
    assert_select "#date", "2022-01-06 14:01:00 UTC"
  end
  
  test "should get add_record" do #пользователь получит доступ к странице для создания новой записи
    start
    get main_view_all_url, params: { act: "add_record"}
    follow_redirect!
    assert_select "h1", "New record"
  end
  
  test "doctor should get view_pat" do # пользователь 'doctor' получит доступ к странице с записями конкретного пациента
    user=User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Суриков', patr: 'Александрович'
    Datum.create username: 'alex',data: '2022-01-06 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
    doctor=User.create username: DOCTOR, password: '1', name: 'Алексей', surname: 'Иванов', patr: 'Александрович'
    post session_create_url, params: { username: doctor.username, password: doctor.password }
    get main_view_all_url, params: { act: "find_pat", name: 'Алексей', surname: 'Суриков', patr: 'Александрович'}
    assert_response :success
    assert_select "#data", "Data: Суриков Алексей Александрович"
    assert_select "td", "alex"
  end
  
  test "doctor should not get view_pat" do # пользователь 'doctor' не получит доступ к странице с записями конкретного пациента, если введены не все данные
    user=User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Суриков', patr: 'Александрович'
    Datum.create username: 'alex',data: '2022-01-06 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
    doctor=User.create username: DOCTOR, password: '1', name: 'Алексей', surname: 'Иванов', patr: 'Александрович'
    post session_create_url, params: { username: doctor.username, password: doctor.password }
    get main_view_all_url, params: { act: "find_pat", name: '', surname: 'Суриков', patr: 'Александрович'}
    follow_redirect!
    assert_select "div", "Enter all information"
  end
  
  test "doctor should not get view_pat 2" do # пользователь 'doctor' не получит доступ к странице с записями конкретного пациента, если введены не все данные
    user=User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Суриков', patr: 'Александрович'
    Datum.create username: 'alex',data: '2022-01-06 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
    doctor=User.create username: DOCTOR, password: '1', name: 'Алексей', surname: 'Иванов', patr: 'Александрович'
    post session_create_url, params: { username: doctor.username, password: doctor.password }
    get main_view_all_url, params: { act: "find_pat", name: '', surname: 'Иванов', patr: 'Александрович'}
    follow_redirect!
    assert_select "div", "Enter all information"
  end
  
  test "not a doctor should not get view_pat" do # пользователь не 'doctor' не получит доступ к странице с записями другого пациента
    start
    get main_view_all_url, params: { act: "find_pat", name: 'Алексей', surname: 'Суриков', patr: 'Александрович'}
    assert_redirected_to new_datum_url
  end
end
