require "application_system_test_case"

class SysTestsTest < ApplicationSystemTestCase
  test "new user" do # создание нового пользователя
    visit "/"
    click_on "Sign up"
    fill_in "user_username", with: 'alex'
    fill_in "user_password", with: '1'
    fill_in "user_name", with: 'Алексей'
    fill_in "user_surname", with: 'Бондаренко'
    fill_in "user_patr", with: 'Александрович'
    click_on "Save"
    assert_selector "p", text: "User was successfully created."
  end
  
  test "user not found" do #незарегистрированный пользователь не сможет войти и получит соответсвующее сообщение
    visit "/"
    fill_in "username", with: 'alex'
    fill_in "password", with: '1'
    click_on "Enter"
    assert_selector "div", text: "User not found"
  end
  
  test "new record" do # зарегистрированнный пользователь может добавить запись
    visit "/"
    new_user=User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    new_user.save
    fill_in "username", with: 'alex'
    fill_in "password", with: '1'
    click_on "Enter"
    visit "/main/view_all?&act=add_record&commit=Choose"
    fill_in "datum_data", with: '2022-01-06 12:20:00'
    fill_in "datum_temp", with: '37.1'
    fill_in "datum_h_pres", with: '135'
    fill_in "datum_l_pres", with: '75'
    click_on "Save"
    assert_selector "p", text: "Record was successfully created."
  end
  
  test "all records" do # зарегистрированный пользователь может получить доступ к своим записям
    visit "/"
    new_user=User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    new_user.save
    fill_in "username", with: 'alex'
    fill_in "password", with: '1'
    click_on "Enter"
    visit "/main/view_all?&act=add_record&commit=Choose"
    fill_in "datum_data", with: '2022-01-06 12:20:00'
    fill_in "datum_temp", with: '37.1'
    fill_in "datum_h_pres", with: '135'
    fill_in "datum_l_pres", with: '75'
    click_on "Save"
    click_on "Back"
    click_on "Back"
    click_on "Choose"
    assert_selector "h1", text: "Records"
    assert_selector "td", text: "Бондаренко"
  end
  
  test "view date" do # зарегистрированный пользователь может получить доступ к своим записям за определённую дату
    visit "/"
    new_user=User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    new_user.save
    fill_in "username", with: 'alex'
    fill_in "password", with: '1'
    click_on "Enter"
    visit "/main/view_all?&act=find_date&date=2022-01-07&commit=Choose"
    assert_selector "h1", text: "Data: 2022-01-07"
    assert_no_selector "td"
  end
  
  test "actual record" do # зарегистрированный пользователь получает правильную актуальную запись
    visit "/"
    new_user=User.create username: 'alex', password: '1', name: 'Алексей', surname: 'Бондаренко', patr: 'Александрович'
    new_user.save
    fill_in "username", with: 'alex'
    fill_in "password", with: '1'
    click_on "Enter"
    new_rec=Datum.create username: 'alex',data: '2022-01-06 14:01:00 UTC', temp: '37.2', h_pres: '130', l_pres:'81'
    new_rec.save
    click_on "Update"
    assert_selector "#actual_record_div", text: "2022-01-06 14:01 37.2 130/81"
  end
end
