class AddDetailsToVisits < ActiveRecord::Migration[5.2]
  def change
    add_column :visits, :date_end, :datetime
    add_column :visits, :diagnosis, :text
    add_column :visits, :anamnesis, :text
  end
end
