class CreateData < ActiveRecord::Migration[5.2]
  def change
    create_table :data do |t|
      t.string :username
      t.datetime :data
      t.float :temp
      t.integer :h_pres
      t.integer :l_pres

      t.timestamps
    end
  end
end
