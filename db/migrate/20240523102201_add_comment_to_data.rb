class AddCommentToData < ActiveRecord::Migration[5.2]
  def change
    add_column :data, :comment, :text
  end
end
