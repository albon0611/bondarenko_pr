class CreateVisits < ActiveRecord::Migration[5.2]
  def change
    create_table :visits do |t|
      t.datetime :date
      t.string :user
      t.text :compl
      t.text :res
      t.text :recom
      t.datetime :next_v

      t.timestamps
    end
  end
end
