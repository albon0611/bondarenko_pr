class AddDoctorToVisits < ActiveRecord::Migration[5.2]
  def change
    add_column :visits, :doctor, :string
  end
end
