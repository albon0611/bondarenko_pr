# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2024_05_23_102201) do

  create_table "data", force: :cascade do |t|
    t.string "username"
    t.datetime "data"
    t.float "temp"
    t.integer "h_pres"
    t.integer "l_pres"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "satur"
    t.integer "pulse"
    t.text "comment"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "password_digest"
    t.string "name"
    t.string "surname"
    t.string "patr"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "polis"
    t.date "birth"
    t.string "role"
  end

  create_table "visits", force: :cascade do |t|
    t.datetime "date"
    t.string "user"
    t.text "compl"
    t.text "res"
    t.text "recom"
    t.datetime "next_v"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "doctor"
    t.datetime "date_end"
    t.text "diagnosis"
    t.text "anamnesis"
  end

end
